using UnityEngine;

public class Breakable : MonoBehaviour 
{
	private float collisionThresh = 0.8f;
	private int leftStrikes = 1;
	private Vector3 startPosition;
	private Transform _transform;
	private bool sonicInside = false;
	
    void OnCollisionEnter(Collision collisionInfo) 
	{
        foreach (ContactPoint contact in collisionInfo.contacts) 
		{
			if(Mathf.Abs(contact.normal.y)>collisionThresh)
			{
				if(!sonicInside)
				{
					if(leftStrikes==0)
						gameObject.transform.position = Vector3.zero;
					else
					{
						sonicInside = true;
						leftStrikes--;
					}
				}
			}
		}
	}
	
    void OnCollisionExit(Collision collisionInfo) 
	{
		sonicInside = false;
	}
	
	void Start()
	{
		_transform = transform.GetChild(0);
		startPosition = _transform.position;
	}
	
	void FixedUpdate()
	{
		if(gameObject.transform.position == Vector3.zero)
		{
			Destroy(gameObject);
		}		
		if(leftStrikes == 0)
		{
			float r = 0.1f;
			_transform.position = startPosition + new Vector3(Random.Range(-r,r),Random.Range(-r,r),0);
		}
	}
}

