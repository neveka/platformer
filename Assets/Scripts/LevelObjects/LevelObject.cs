﻿using UnityEngine;

public interface ILevelObject
{
    void SetVisible(bool visible);
    int GetId();
}

public class LevelObject : MonoBehaviour, ILevelObject
{
    public virtual void SetVisible(bool visible)
    {
        gameObject.SetActive(visible);
    }

    public virtual int GetId()
    {
        return gameObject.GetInstanceID();
    }
}