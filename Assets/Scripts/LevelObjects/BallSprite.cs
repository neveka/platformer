using SpriteUtils;
using UnityEngine;

public class BallSprite : Throwable 
{
	public SpriteManager spriteMan;
	private SpriteUtils.Sprite sprite;

	void Awake () 
	{
		sprite = spriteMan.AddSprite(gameObject, // The game object to associate the sprite to
										0.8f, 		// The width of the sprite
										0.4f, 		// The height of the sprite
										382+15,//404, 		// Left pixel
										338+35,//303, 		// Bottom pixel
										40, 		// Width in pixels
										20, 		// Height in pixels
										false);		// Billboarded?
	}
	
	public override void Fire(Vector3 position, bool right)
	{
		gameObject.SetActive(true);
		base.Fire(position, right);
		transform.localScale = new Vector3(right?1:-1, 1, 1);
		sprite.hidden = false;
	}
	
	public override void Reset()
	{
        sprite.hidden = true;	
		base.Reset();
	}
}
		
