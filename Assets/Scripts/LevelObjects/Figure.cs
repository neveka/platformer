using UnityEngine;

public class Figure : Throwable, ILevelObject
{
	public float side = 0.4f;
	public GameObject [] cubes;
	public Vector2 [] positions;
	public Color color;
	private Color [] colors = {Color.red, Color.blue, Color.green, Color.yellow, Color.magenta, Color.cyan};
	
	bool PlaceFound(int idx, Vector2 position)
	{
		bool isBusy = false;
		bool hasNeibour = false;
		for(int i=0; i<idx; i++)
			if(positions[i]==position)
				isBusy = true;
		for(int i=0; i<idx; i++)
			if((positions[i]-position).magnitude == 1)
				hasNeibour = true;
		return !isBusy && hasNeibour;
	}
	
	void Start ()
    {
		//GetComponent<Rigidbody>().enabled = false;
		positions = new Vector2[cubes.Length];
		color = colors[Random.Range(0, colors.Length)];
		for(int i=0; i<cubes.Length; i++)
		{
			cubes[i].GetComponent<Renderer>().material.SetColor("_Color" , color);
			if(i==0)
				positions[0] = new Vector2(0, 0);
			else
			{
				bool placeFound = false;
				while(!placeFound)
				{
					positions[i] = new Vector2(Random.Range(-2,3), Random.Range(-2,3));
					placeFound = PlaceFound(i, positions[i]);
 				}
				//cubes[i].transform.position += new Vector3(positions[i].x*side, positions[i].y*side, 0);
			}
		}
		SyncCubes();
	}
	
	public void SyncCubes()
	{
		for(int i=0; i<cubes.Length; i++)
			cubes[i].transform.localPosition = new Vector3((positions[i].x+0.5f)*side, (positions[i].y+0.5f)*side, 0);		
	}
	
	public void SyncPositions()
	{
		for(int i=0; i<cubes.Length; i++)
			positions[i] = new Vector2((int)((cubes[i].transform.localPosition.x-0.5f*side)/side), (int)((cubes[i].transform.localPosition.y-0.5f*side)/side));		
	}
	
	public void Move(int x, int y)
	{
		for(int i=0; i<cubes.Length; i++)
		{
			positions[i].x+=x;
			positions[i].y+=y;
		}
		//SyncCubes();
	}
	
	public override void Fire(Vector3 position, bool right)
	{
		GetComponent<Rigidbody>().useGravity = true;
        gameObject.SetActive(true);
		GetComponent<Collider>().isTrigger = false;		
		burnDuration = 0;
		base.Fire(position, right);
	}
	
	void OnTriggerEnter(Collider other)
	{
		if(other.gameObject.GetComponent<Tetris>())
		{
			Reset();
			transform.parent = other.gameObject.transform;
			other.gameObject.GetComponent<Tetris>().AddFigure(this);
			Destroy(GetComponent<Rigidbody>());
			Destroy(GetComponent<Collider>());
		}
	}

    public void SetVisible(bool visible)
    {
        gameObject.SetActive(visible);
    }

    public int GetId()
    {
        return gameObject.GetInstanceID();
    }

    public void SetParent(ICharacterVisual walkerVisual)
    {
        walkerVisual.AddFigure(this);
    }
}
