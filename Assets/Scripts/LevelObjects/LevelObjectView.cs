﻿using System;
using UnityEngine;

namespace Lessons
{
    public class LevelObjectView : MonoBehaviour
    {
        public SpriteRenderer SpriteRenderer;
        public Transform Transform;
        public Collider2D Collider2D;
        public Rigidbody2D Rigidbody2D;

        public Action<LevelObjectView, LevelObjectView> OnLevelObjectContact { get; set; }

        // when the GameObjects collider arrange for this GameObject to travel to the left of the screen
        void OnTriggerEnter2D(Collider2D collider)
        {
            //Debug.Log(collider.gameObject.name + " : " + gameObject.name + " : " + Time.time);
            var levelObject = collider.gameObject.GetComponent<LevelObjectView>();
            OnLevelObjectContact?.Invoke(this, levelObject);
        }
    }
}
