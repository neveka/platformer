﻿using System;
using UnityEngine;

namespace Lessons
{
    public class BallView: LevelObjectView
    {
        [SerializeField]
        private TrailRenderer _trail;

        public bool IsVisible()
        {
            return SpriteRenderer.enabled;
        }

        public void SetVisible(bool visible)
        {
            if (_trail) _trail.enabled = visible;
            if (_trail) _trail.Clear();
            SpriteRenderer.enabled = visible;
        }
    }
}
