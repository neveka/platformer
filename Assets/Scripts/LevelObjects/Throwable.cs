using UnityEngine;

public class Throwable: MonoBehaviour
{
	public float burnDuration = 1f;
	public Vector3 throwForce = new Vector3(500, 10, 0);
	private float burnTime = 0;
	private Vector3 force = Vector3.zero;
	
	void FixedUpdate () 
	{
		if(burnTime>0)
		{	
			burnTime -= Time.fixedDeltaTime;
			if(burnTime<=0)
				Reset();
		}
	}	
	
	public virtual void Fire(Vector3 position, bool right)
	{
		Reset();
		burnTime = burnDuration;
		transform.position = position+new Vector3(right?1:-1,0,0);
		GetComponent<Collider>().enabled = true;
		force = new Vector3(right?throwForce.x:-throwForce.x,throwForce.y,throwForce.z);
		GetComponent<Rigidbody>().AddForce(force);
	}
	
	public virtual void Reset()
	{		
        GetComponent<Collider>().enabled = false;
		force = Vector3.zero;
		GetComponent<Rigidbody>().velocity = Vector3.zero;
        GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
		FixedUpdate();
	}	
}
