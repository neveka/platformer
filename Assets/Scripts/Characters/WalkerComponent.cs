﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public enum ContactType
{
    ground,
    left,
    right
}

public class WalkerComponent : IDisposable
{
    public Action<Portal> OnPortalEntered = null;

    private float walkSpeed = 4;
    private float flySpeed = 3;
    private float jumpForse = 400;
    private float movingThresh = 0.1f;
    private float collisionThresh = 0.5f;
    private float flyThresh = 1f;

    private bool _isControlled = true;
    private bool jumpUsed = true;
    private bool doJump = false;
    private float goSideWay = 0;
    private Vector3 _startPosition;

    private HashSet<int>[] _collisions = new HashSet<int>[3];

    private readonly ICharacterVisual _visual;
    private readonly ILevelEvents _levelEvents;

    public WalkerComponent(ICharacterVisual walkerVisual, ILevelEvents levelEvents)
    {
        _visual = walkerVisual;
        _levelEvents = levelEvents;

        _levelEvents.OnStartLevel += OnStart;
        _levelEvents.OnFixedUpdate += OnFixedUpdate;
        _visual.OnContactChanged += OnContactChanged;
        _visual.OnContactRemoved += OnContactRemoved;
        _visual.OnLevelObjectContact += OnLevelObjectContact; 
    }

    public void SetStartPosition(Vector3 startPosition, bool isControlled)
    {
        _isControlled = isControlled;
        _startPosition = startPosition;
        Reset();
    }

    public void Dispose()
    {
        _levelEvents.OnStartLevel -= OnStart;
        _levelEvents.OnFixedUpdate -= OnFixedUpdate;
        _visual.OnContactChanged -= OnContactChanged;
        _visual.OnContactRemoved -= OnContactRemoved;
        _visual.OnLevelObjectContact -= OnLevelObjectContact;
    }

    private void OnStart()
    {
        (_visual.MainSprite as SonicAnimatedSprite).StartAnimation(4, false, 5);
        _visual.MainSprite.Turn(false);
    }

    private void OnFixedUpdate()
    {
        doJump = _isControlled && Input.GetAxis("Vertical") > 0;
        goSideWay = _isControlled ? Input.GetAxis("Horizontal") : 0;
        if (IsGrounded())
        {
            //walking
            if (Mathf.Abs(goSideWay) > movingThresh)
            {
                SetXVelocity((goSideWay > 0 ? -1 : 1) * walkSpeed);
                _visual.MainSprite.Turn(goSideWay < 0);
                (_visual.MainSprite as SonicAnimatedSprite).StartAnimation(3, true, 10);
            }
            else
            {
                _visual.MainRigidBody.velocity = Vector3.up * _visual.MainRigidBody.velocity.y;
                (_visual.MainSprite as SonicAnimatedSprite).StartAnimation(0, true, 10);
            }
            //jumping
            if (doJump && !jumpUsed)
            {
                _visual.MainRigidBody.AddForce(Vector3.up * jumpForse);
                jumpUsed = true;
            }
        }
        else
        {
            jumpUsed = false;

            //flying
            if (Mathf.Abs(goSideWay) > movingThresh && 
                (goSideWay < 0 || _collisions[(int)ContactType.left].Count == 0) && 
                (goSideWay > 0 || _collisions[(int)ContactType.right].Count == 0))
            {
                SetXVelocity((goSideWay > 0 ? -1 : 1) * flySpeed);
                _visual.MainSprite.Turn(goSideWay < 0);
            }
            if (Mathf.Abs(_visual.MainRigidBody.velocity.y) > flyThresh)
            {
                (_visual.MainSprite as SonicAnimatedSprite).StartAnimation(4, false, 5);
            }
            //falling
            if (_visual.Position.y < -10)
                _visual.Position = _startPosition;
        }
    }

    private void SetXVelocity(float x)
    {
        var velocity = _visual.MainRigidBody.velocity;
        velocity.x = x;
        _visual.MainRigidBody.velocity = velocity;
    }

    private void OnContactChanged(Vector3 normal, int id)
    {
        UpdateCollisions(ContactType.ground, id, normal.y > collisionThresh);
        UpdateCollisions(ContactType.left, id, normal.x > collisionThresh);
        UpdateCollisions(ContactType.right, id, normal.x < -collisionThresh);

        if (_collisions[(int)ContactType.left].Count > 0 && _visual.MainRigidBody.velocity.x < 0) SetXVelocity(0);
        if (_collisions[(int)ContactType.right].Count > 0 && _visual.MainRigidBody.velocity.x > 0) SetXVelocity(0);
    }

    private void UpdateCollisions(ContactType contactType, int id, bool add)
    {
        var list = _collisions[(int)contactType];
        if (add)
        {
            if(!list.Contains(id)) list.Add(id);
        }
        else
        {
            list.Remove(id);
        }
    }

    private void OnContactRemoved(int id)
    {
        for (int i = 0; i < _collisions.Length; i++)
        {
            _collisions[i].Remove(id);
        }
    }

    private void OnLevelObjectContact(Vector3 normal, ILevelObject levelObject)
    {
        if (levelObject is DragonSprite || levelObject is CharacterLevelObject)
        {
            if (normal.y > collisionThresh)
            {
                OnContactRemoved(levelObject.GetId());
                levelObject.SetVisible(false);
            }
            else
            {
                Reset();
            }
        }

        if (levelObject is Portal)
		{
			OnPortalEntered(levelObject as Portal);
            levelObject.SetVisible(false);
        }
    }

    private void Reset()
    {
        //Debug.Log("Reset");
        for (int i = 0; i < _collisions.Length; i++)
        {
            if(_collisions[i] == null) _collisions[i] = new HashSet<int>();
            _collisions[i].Clear();
        }

        _visual.Position = _startPosition;
        _visual.MainRigidBody.velocity = Vector3.zero;
        _visual.MainRigidBody.angularVelocity = Vector3.zero;
        doJump = false;
        jumpUsed = false;
        goSideWay = 0;
        _visual.MainSprite.Turn(false);
    }

    public bool IsGrounded()
    {
        return _collisions[(int)ContactType.ground].Count > 0;
    }
}
