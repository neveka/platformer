using UnityEngine;
using System.Collections;
using System;

public class EnemyComponent: IDisposable
{
	private float walkSpeed = 3;
	private float goSideWay = -1;
	private float collisionThresh = 0.3f;

    private readonly ICharacterVisual _visual;
    private readonly ILevelEvents _levelEvents;

    public EnemyComponent(ICharacterVisual walkerVisual, ILevelEvents levelEvents)
    {
        _visual = walkerVisual;
        _levelEvents = levelEvents;
        levelEvents.OnFixedUpdate += OnFixedUpdate;
        _visual.OnContactChanged += OnContactChanged;
    }

    public void Dispose()
    {
        _levelEvents.OnFixedUpdate -= OnFixedUpdate;
        _visual.OnContactChanged -= OnContactChanged;
    }

    void OnFixedUpdate () 
	{
        var velocity = _visual.MainRigidBody.velocity;
        velocity.x = (goSideWay > 0 ? -1 : 1) * walkSpeed;
        _visual.MainRigidBody.velocity = velocity;
        _visual.MainSprite.Turn(goSideWay>0);
	}

    private void OnContactChanged(Vector3 normal, int id)
    {
        if (normal.x > collisionThresh)
            goSideWay = -1;
        if (normal.x < -collisionThresh)
            goSideWay = 1;
    }
}
