using UnityEngine;
using System;
using System.Collections.Generic;
using SpriteUtils;

public interface ICharacterVisual
{
    Action<Vector3, int> OnContactChanged { get; set; }
    Action<int> OnContactRemoved { get; set; }
    Action<Vector3, ILevelObject> OnLevelObjectContact { get; set; }

    AnimatedSprite MainSprite { get; }
    Rigidbody MainRigidBody { get; }
    Vector3 Position { get; set; }
    Camera Camera { get; }
    List<Throwable> Bullets { get; }
    void AddFigure(ILevelObject levelObject);
}

public class CharacterLevelObject : LevelObject, ICharacterVisual
{
    public Action<Vector3, int> OnContactChanged { get; set; }
    public Action<int> OnContactRemoved { get; set; }
    public Action<Vector3, ILevelObject> OnLevelObjectContact { get; set; }

    [SerializeField]
	private AnimatedSprite _sprite;
    [SerializeField]
    private Rigidbody _rigidbody;
    [SerializeField]
    private Camera _camera;
    [SerializeField]
    private List<Throwable> _bullets = new List<Throwable>();

    public AnimatedSprite MainSprite => _sprite;
    public Rigidbody MainRigidBody => _rigidbody;
    public Camera Camera => _camera;
    public List<Throwable> Bullets => _bullets;
    public Vector3 Position
    {
        set { transform.position = value; }
        get { return transform.position; }
    }

    public void AddFigure(ILevelObject levelObject)
    {
        if(levelObject is Figure) (levelObject as Figure).transform.parent = transform;
    }

    void OnCollisionStay(Collision collisionInfo)
	{	
 		foreach (ContactPoint contact in collisionInfo.contacts) 
		{
            OnContactChanged?.Invoke(contact.normal, collisionInfo.gameObject.GetInstanceID());
        }
	}	
	
    void OnCollisionEnter(Collision collisionInfo) 
	{
        OnCollisionStay(collisionInfo);

        if (collisionInfo.gameObject.transform.parent!=transform)
		{
            var levelObject = collisionInfo.gameObject.GetComponent<ILevelObject>();
            if (levelObject != null) OnLevelObjectContact?.Invoke(collisionInfo.contacts[0].normal, levelObject);
		}		
	}

    void OnCollisionExit(Collision collisionInfo)
    {
        OnContactRemoved?.Invoke(collisionInfo.gameObject.GetInstanceID());
    }

    void OnTriggerEnter(Collider collisionInfo)
	{
        if (collisionInfo.gameObject.transform.parent != transform)
        {
            var levelObject = collisionInfo.gameObject.GetComponent<ILevelObject>();
            if (levelObject != null) OnLevelObjectContact?.Invoke(Vector3.zero, levelObject);
        }	
	}	
}