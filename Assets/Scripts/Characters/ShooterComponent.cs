using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ShooterComponent
{
    public System.Action<int> OnFiguresCountChanged;

    private List<Throwable> _figures = new List<Throwable>();
    private ICharacterVisual _visual;
    private WalkerComponent _walkerComponent;
	private Tetris _tetris;
	private int index = 0;
	private float fireDelay = 0; 

	public ShooterComponent(ICharacterVisual visual, WalkerComponent walkerComponent, ILevelEvents levelEvents)
	{
        _visual = visual;
        _walkerComponent = walkerComponent;

        levelEvents.OnFixedUpdate += OnFixedUpdate;
        _visual.OnLevelObjectContact += OnLevelObjectContact;
    }

    public void SetTetris(Tetris tetris)
    {
        _tetris = tetris;
    }
	
    void OnFixedUpdate() 
	{
		if(_tetris)
		{
			if(!_tetris.busy && _walkerComponent.IsGrounded() && _visual.MainSprite.animationTrack == (int)SonicAnimatedSprite.Animations.walk)
			{
				OnFiguresCountChanged(_figures.Count);
				_tetris.busy = true;
				if(_figures.Count>0)
				{
					_figures[0].Fire(_visual.Position, _visual.MainSprite.transform.localScale.x>0);
					_figures.RemoveAt(0);
				}
			}
		}	
		else
		{
			if(fireDelay>0)
				fireDelay-=Time.fixedDeltaTime;			
			if(Input.GetButton("Jump"))
			{
				if(fireDelay<=0)
				{
					if(index>= _visual.Bullets.Count)
						index = 0;
                    _visual.Bullets[index].Fire(_visual.Position, _visual.MainSprite.transform.localScale.x>0);	
					index++;					
					fireDelay = 0.5f;
				}
			}
			else
				fireDelay = 0;
		}
    }

    private void OnLevelObjectContact(Vector3 normal, ILevelObject levelObject)
    {
        if (levelObject is Figure)
		{
			_figures.Add(levelObject as Figure);
			OnFiguresCountChanged(_figures.Count);
            (levelObject as Figure).SetParent(_visual);
            levelObject.SetVisible(false);
	    }
	}	
}