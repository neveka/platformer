using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Tetris : MonoBehaviour 
{
	public bool busy = false;
    public float side = 0.3f;
    public GameObject cubePrefab;
	public int filledLines = 4;
	public System.Action OnFailed = null;
    private GameObject[][] _matrix = null;
	private Figure _figure;
	private float _tickDuration = 0.5f;
	private float _timeTillTick = 0.5f;
	private float _moveSide = 0;
	private float _moveDelay = 0;
	private float _rotateDelay = 0;

	void Start () 
	{
		int w = (int)(GetComponent<BoxCollider>().size.x/side);
		int h = (int)(GetComponent<BoxCollider>().size.y/side)+5;//?
		Debug.Log("Tetris dimentions "+w+" "+h);
		_matrix = new GameObject[w][];
		for (int x = 0; x < w; x++) 
		{
   			_matrix [x] = new GameObject[h];
			for(int y=0; y<h; y++)
			{
				if(y>filledLines || Random.Range(0,2)==0)
					_matrix[x][y] = null;
				else
				{
					GameObject cube = (GameObject)Instantiate(cubePrefab);
					cube.transform.parent = transform;
					cube.transform.localPosition = new Vector3((x+0.5f)*side, (y+0.5f)*side, 0);
					_matrix[x][y] = cube;
				}
			}
		}
	}
	
	void HandleInput()
	{
		_moveSide = Input.GetAxis("Horizontal");
		//Debug.Log(Input.GetKey ("left")+" "+_moveSide +" "+_moveDelay );
		if(_moveSide!=0 && Input.GetKey ("left")||Input.GetKey ("right"))
		{
			if(_moveDelay<=0)
			{
				_figure.Move(_moveSide>0?-1:1, 0);
				if(IsPossible())
					_figure.SyncCubes();
				else
					_figure.SyncPositions();
				_moveSide = 0;
				_moveDelay = _tickDuration;
			}
		}	
		else
			_moveDelay = 0;

		if(Input.GetAxis("Vertical")>0 && Input.GetKey ("up"))
		{
			if(_rotateDelay<=0)
			{	
				for(int i=1; i<_figure.cubes.Length; i++)
				{
					float temp = _figure.positions[i].x;
					_figure.positions[i].x = _figure.positions[0].x+(_figure.positions[i].y-_figure.positions[0].y);
					_figure.positions[i].y = _figure.positions[0].y-(temp-_figure.positions[0].x);
				}
				if(IsPossible())
					_figure.SyncCubes();
				else
					_figure.SyncPositions();				
				_rotateDelay = _tickDuration;
			}
		}
		else if(Input.GetAxis("Vertical")<0)
		{
			while(IsPossible())
			{
				_figure.SyncCubes();
				_figure.Move(0, -1);
			}
			_figure.SyncPositions();
		}
		else
			_rotateDelay = 0;		
	}
	
	void Update () 
	{
		if(_moveDelay>0)
			_moveDelay-=Time.deltaTime;
		if(_rotateDelay>0)
			_rotateDelay-=Time.deltaTime;
		if(_figure)
			HandleInput();
		
		if(_timeTillTick>0)
		{
			_timeTillTick-= Time.deltaTime;
			if(_timeTillTick<=0)
			{
				Tick();
				_timeTillTick = _tickDuration;
			}
		}		
	}
	
	public void AddFigure(Figure figure)
	{
		_figure = figure;
        _figure.transform.localScale = new Vector3(side/_figure.side,side/_figure.side,side/_figure.side);
        _figure.side = side;
		for(int i=0; i<_figure.cubes.Length; i++)
			_figure.cubes[i].transform.parent = transform;
		_figure.SyncPositions();
		if(IsPossible())
			_figure.SyncCubes();
		else
		{
			_figure.gameObject.SetActive(false);
			_figure = null;//?
			OnFailed();
		}
	}
	
	bool IsPossible()
	{
		if(!_figure)
			return false;
		bool canMove = true;
		for(int i=0; i<_figure.cubes.Length; i++)
		{
			//Debug.Log(_figure.positions[i].x+"+"+x+" "+_figure.positions[i].y+"+"+y);
			if(_figure.positions[i].x<0 || 
			   _figure.positions[i].x>=_matrix.Length || 
			   _figure.positions[i].y<0 || 
			   _figure.positions[i].y>=_matrix[0].Length || 
				_matrix[(int)_figure.positions[i].x][(int)_figure.positions[i].y] )
				canMove = false;
		}
		return canMove;
	}
	
	void RemoveFilledLines()
	{
		for(int i=0; i<_matrix[0].Length; i++)
		{
			bool filled = true;
			for(int j=0; j<_matrix.Length; j++)
				if(_matrix[j][i] == null)
				{
					//Debug.Log(j+" "+i+" not filled");
					filled = false;
					break;
				}
			if(filled)
			{
				//Debug.Log(i+ " filled");
				for(int k=i; k<_matrix[0].Length-1; k++)
					for(int j=0; j<_matrix.Length; j++)
					{
						if(k==i)
							Destroy(_matrix[j][k]);
						_matrix[j][k] = _matrix[j][k+1];
						if(_matrix[j][k])
							_matrix[j][k].transform.position += Vector3.down*side;
					}
				//for(int j=0; j<_matrix.Length; j++) ???
				//	_matrix[j][_matrix[0].Length-1] = null;
			}
		}		
	}
	
	void DebugMatrix()
	{
		string upperLine = "";
		for(int i=0; i<_matrix[0].Length; i++)
		{
		for(int j=0; j<_matrix.Length; j++)
			upperLine += _matrix[j][i]?1:0;
			upperLine += "\r\n";
		}
		Debug.Log(upperLine);
	}
	
	void Tick()
	{
		if(_figure)
		{
			_figure.Move(0, -1);
			if(!IsPossible())
			{
				_figure.SyncPositions();
				for(int i=0; i<_figure.cubes.Length; i++)
					_matrix[(int)_figure.positions[i].x][(int)_figure.positions[i].y] = _figure.cubes[i];
				_figure = null;
				busy = false;
				
				RemoveFilledLines();
			}
			else
				_figure.SyncCubes();
		}
	}
}
