﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Lessons
{
    public class CoinsManager : IDisposable
    {
        private const float _animationsSpeed = 10;

        private LevelObjectView _characterView;
        private SpriteAnimator _spriteAnimator;
        private List<LevelObjectView> _coinViews;

        public CoinsManager(LevelObjectView characterView, List<LevelObjectView> coinViews, SpriteAnimator spriteAnimator)
        {
            _characterView = characterView;
            _spriteAnimator = spriteAnimator;
            _coinViews = coinViews;
            _characterView.OnLevelObjectContact += OnLevelObjectContact;

            foreach (var coinView in coinViews)
            {
                _spriteAnimator.StartAnimation(coinView.SpriteRenderer, Track.coin_rotation, true, _animationsSpeed);
            }
        }

        private void OnLevelObjectContact(LevelObjectView thisView, LevelObjectView otherView)
        {
            if (_coinViews.Contains(otherView))
            {
                _spriteAnimator.StopAnimation(otherView.SpriteRenderer);
                GameObject.Destroy(otherView.gameObject);
            }
        }

        public void Dispose()
        {
            _characterView.OnLevelObjectContact -= OnLevelObjectContact;
        }
    }
}
