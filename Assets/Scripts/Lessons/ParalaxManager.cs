﻿using UnityEngine;

namespace Lessons
{
    public class ParalaxManager
    {
        private Camera _camera;
        private Transform _back;
        private Vector3 _backStartPosition;
        private Vector3 _cameraStartPosition;
        private const float _coef = 0.3f;

        public ParalaxManager(Camera camera, Transform back)
        {
            _camera = camera;
            _back = back;
            _backStartPosition = _back.transform.position;
            _cameraStartPosition = _camera.transform.position;
        }

        public void Update()
        {
            _back.position = _backStartPosition + (_camera.transform.position - _cameraStartPosition) * _coef;
        }
    }
}
