﻿using UnityEngine;

namespace Lessons
{
    public class MainHeroWalker
    {
        private const float _walkSpeed = 3;
        private const float _animationsSpeed = 10;
        private const float _jumpStartSpeed = 8;
        private const float _movingThresh = 0.1f;
        private const float _flyThresh = 1f;
        private const float _groundLevel = 0.5f;
        private const float _g = -10;

        private Vector3 _leftScale = new Vector3(-1, 1, 1);
        private Vector3 _rightScale = new Vector3(1, 1, 1);

        private float _yVelocity;
        private bool _doJump;
        private float xAxisInput;

        private LevelObjectView _view;
        private SpriteAnimator _spriteAnimator;

        public MainHeroWalker(LevelObjectView view, SpriteAnimator spriteAnimator)
        {
            _view = view;
            _spriteAnimator = spriteAnimator;
        }

        public void Update()
        {
            _doJump = Input.GetAxis("Vertical") > 0;
            xAxisInput = Input.GetAxis("Horizontal");
            var goSideWay = Mathf.Abs(xAxisInput) > _movingThresh;

            if (IsGrounded())
            {
                //walking
                if (goSideWay) GoSideWay();
                _spriteAnimator.StartAnimation(_view.SpriteRenderer, goSideWay ? Track.sonic_walk : Track.sonic_stand, true, _animationsSpeed);

                //start jump
                if (_doJump && _yVelocity == 0)
                {
                    _yVelocity = _jumpStartSpeed;
                }
                //stop jump
                else if (_yVelocity < 0)
                {
                    _yVelocity = 0;
                    _view.Transform.position = _view.Transform.position.Change(y: _groundLevel);
                }
            }
            else
            {
                //flying
                if (goSideWay) GoSideWay();
                if (Mathf.Abs(_yVelocity) > _flyThresh)
                {
                    _spriteAnimator.StartAnimation(_view.SpriteRenderer, Track.sonic_jump, true, _animationsSpeed);
                }
                _yVelocity += _g * Time.deltaTime;
                _view.Transform.position += Vector3.up * Time.deltaTime * _yVelocity;
            }
        }

        private void GoSideWay()
        {
            _view.Transform.position += Vector3.right * Time.deltaTime * _walkSpeed * (xAxisInput < 0 ? -1 : 1);
            _view.Transform.localScale = (xAxisInput < 0 ? _leftScale : _rightScale);
        }

        public bool IsGrounded()
        {
            return _view.Transform.position.y <= _groundLevel + float.Epsilon && _yVelocity <= 0;
        }
    }
}
