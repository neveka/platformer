﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Lessons
{
    class EnemyWalker
    {
        private const float _animationsSpeed = 10;
        private const float _walkSpeed = 50;
        private readonly Vector3 _leftScale = new Vector3(1, 1, 1);
        private readonly Vector3 _rightScale = new Vector3(-1, 1, 1);

        private float _goSideWay = -1;

        private readonly LevelObjectView _view;
        private readonly SpriteAnimator _spriteAnimator;
        private readonly ContactsPoller _contactsPoller;

        public EnemyWalker(LevelObjectView view, SpriteAnimator spriteAnimator)
        {
            _view = view;
            _spriteAnimator = spriteAnimator;
            _contactsPoller = new ContactsPoller(view.Collider2D);
        }

        public void FixedUpdate()
        {
            _contactsPoller.Update();

            if (_goSideWay > 0 && _contactsPoller.HasRightContacts) _goSideWay = -1;
            else if(_goSideWay < 0  && _contactsPoller.HasLeftContacts) _goSideWay = 1;

            _view.Rigidbody2D.velocity = _view.Rigidbody2D.velocity.Change(x: Time.fixedDeltaTime * _walkSpeed * _goSideWay);
            _view.Transform.localScale = (_goSideWay < 0 ? _leftScale : _rightScale);
            _spriteAnimator.StartAnimation(_view.SpriteRenderer, Track.enemy_walk, true, _animationsSpeed);
        }
    }
}
