﻿using UnityEngine;

namespace Lessons
{
    public class Bullet
    {
        private float _radius = 0.3f;
        private Vector3 _velocity;

        private float _groundLevel = 0;
        private float _g = -10;
        private float _windage = 3;
        private float _elasticity = 0.7f;

        private BallView _view;

        public Bullet(BallView view)
        {
            _view = view;
            _view.SetVisible(false);
        }

        public void Update()
        {
            if (IsGrounded())
            {
                var normal = Vector3.up;
                var outVelocity = Quaternion.AngleAxis(180, normal) * -_velocity;
                var outNornalVelocity = Vector3.Dot(outVelocity, normal) * normal;
                var outNotNormalVelocity = outVelocity - outNornalVelocity;
                var resultVelocity = outNotNormalVelocity + outNornalVelocity * _elasticity;

                SetVelocity(resultVelocity);
                _view.Transform.position = _view.Transform.position.Change(y: _groundLevel + _radius);
            }
            else
            {
                SetVelocity(_velocity - _velocity.normalized * _windage * Time.deltaTime + Vector3.up * _g * Time.deltaTime);
                _view.Transform.position += _velocity * Time.deltaTime;
            }
        }

        public void Throw(Vector3 position, Vector3 velocity)
        {
            _view.SetVisible(false);
            _view.Transform.position = position;
            SetVelocity(velocity);
            _view.SetVisible(true);
        }

        private void SetVelocity(Vector3 velocity)
        {
            _velocity = velocity;
            var angle = Vector3.Angle(Vector3.left, _velocity);
            var axis = Vector3.Cross(Vector3.left, _velocity);
            var newAngle = Quaternion.AngleAxis(angle, axis);

            _view.Transform.rotation = !_view.IsVisible()? newAngle : Quaternion.Lerp(_view.Transform.rotation, newAngle, Time.deltaTime * 4);
        }

        public bool IsGrounded()
        {
            return _view.Transform.position.y <= _groundLevel + _radius + float.Epsilon && _velocity.y <= 0;
        }
    }
}
