﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Lessons
{
    public class LiftsManager : IDisposable
    {
        private List<LevelObjectView> _liftViews;
        private List<LevelObjectView> _turnTriggers;
        private JointMotor2D _jointMotor;

        public LiftsManager(List<LevelObjectView> liftViews, List<LevelObjectView> turnTriggers)
        {
            _liftViews = liftViews;
            _turnTriggers = turnTriggers;
            foreach (var liftView in liftViews)
            {
                liftView.OnLevelObjectContact += OnLevelObjectContact;
            }
        }

        public void Dispose()
        {
            foreach (var liftView in _liftViews)
            {
                liftView.OnLevelObjectContact -= OnLevelObjectContact;
            }
        }

        private void OnLevelObjectContact(LevelObjectView thisObject, LevelObjectView otherObject)
        {
            if (_turnTriggers.Contains(otherObject))
            {
                var liftJoint = thisObject.GetComponent<SliderJoint2D>();
                _jointMotor.motorSpeed = liftJoint.motor.motorSpeed * -1;
                _jointMotor.maxMotorTorque = liftJoint.motor.maxMotorTorque;
                liftJoint.motor = _jointMotor;
            }
        }
    }
}
