﻿using System.Collections.Generic;
using UnityEngine;

namespace Lessons
{
    public class BulletsEmitter
    {
        private const float _delay = 1;
        private const float _startSpeed = 10;

        private List<PhysicsBullet> _bullets = new List<PhysicsBullet>();
        private Transform _transform;

        private int _currentIndex;
        private float _timeTillNextBullet;

        public BulletsEmitter(List<BallView> ballViews, Transform transform)
        {
            _transform = transform;
            _timeTillNextBullet = _delay;
            if (ballViews == null) return;
            foreach (var ballView in ballViews)
            {
                _bullets.Add(new PhysicsBullet(ballView));
            }
        }

        public void FixedUpdate()
        {
            if (_timeTillNextBullet > 0)
            {
                _timeTillNextBullet -= Time.deltaTime;
            }
            else if(_bullets.Count > 0)
            {
                _timeTillNextBullet = _delay;
                _bullets[_currentIndex].Throw(_transform.position, _transform.up * _startSpeed);
                _currentIndex++;
                if (_currentIndex >= _bullets.Count) _currentIndex = 0;
            }
            //_bullets.ForEach(b => b.Update());
        }
    }
}
