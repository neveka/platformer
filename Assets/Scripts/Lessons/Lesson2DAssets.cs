﻿using System.Collections.Generic;
using UnityEngine;
using static Lessons.LiftsManager;

namespace Lessons
{
    public class Lesson2DAssets : MonoBehaviour
    {
        [SerializeField]
        private LevelObjectView _characterView;
        [SerializeField]
        private LevelObjectView _enemyView;
        [SerializeField]
        private List<BallView> _ballViews;
        [SerializeField]
        private List<LevelObjectView> _coinViews;
        [SerializeField]
        private List<LevelObjectView> _deathZones;
        [SerializeField]
        private List<LevelObjectView> _winZones;
        [SerializeField]
        private List<LevelObjectView> _lifts;
        [SerializeField]
        private List<LevelObjectView> _liftsTriggers;
        [SerializeField]
        private CannonView _cannonView;
        [SerializeField]
        private Camera _camera;
        [SerializeField]
        private Transform _back;
        [SerializeField]
        private bool _withPhysics;

        private SpriteAnimator _spriteAnimator;
        private ParalaxManager _paralaxManager;
        private MainHeroWalker _mainHeroWalker;
        private MainHeroPhysicsWalker _mainHeroPhysicsWalker;
        private EnemyWalker _enemyWalker;
        private AimingMuzzle _aimingMuzzle;
        private CoinsManager _coinsCollector;
        private BulletsEmitter _ballsEmitter;

        private void Start()
        {
            SpriteAnimationsConfig config = Resources.Load<SpriteAnimationsConfig>("SpriteAnimationsConfig");
            _spriteAnimator = new SpriteAnimator(config);
            if (_withPhysics)
            {
                _mainHeroPhysicsWalker = new MainHeroPhysicsWalker(_characterView, _spriteAnimator);
                _enemyWalker = new EnemyWalker(_enemyView, _spriteAnimator);
            }
            else
            {
                _mainHeroWalker = new MainHeroWalker(_characterView, _spriteAnimator);
            }
            _paralaxManager = new ParalaxManager(_camera, _back);
            _aimingMuzzle = new AimingMuzzle(_cannonView.MuzzleTransform, _characterView.Transform);
            _coinsCollector = new CoinsManager(_characterView, _coinViews, _spriteAnimator);
            _ballsEmitter = new BulletsEmitter(_ballViews, _cannonView.EmitterTransform);
            new LevelCompleteManager(_characterView, _deathZones, _winZones);
            new LiftsManager(_lifts, _liftsTriggers);
        }

        private void Update()
        {
            if (Time.timeSinceLevelLoad < 0.5f) return;

            _spriteAnimator.Update();
            _paralaxManager.Update();
            _aimingMuzzle.Update();
            if(!_withPhysics)_mainHeroWalker.Update();  
        }

        private void FixedUpdate()
        {
            if (!_withPhysics) return;

            _mainHeroPhysicsWalker.FixedUpdate();
            _enemyWalker.FixedUpdate();
            _ballsEmitter.FixedUpdate();
        }

        private void OnDestroy()
        {
            _coinsCollector.Dispose();
            _spriteAnimator.Dispose();
        }
    }
}