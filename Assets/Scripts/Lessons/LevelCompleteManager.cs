﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Lessons
{
    public class LevelCompleteManager : IDisposable
    {
        private Vector3 _startPosition;
        private LevelObjectView _characterView;
        private List<LevelObjectView> _deathZones;
        private List<LevelObjectView> _winZones;

        public LevelCompleteManager(LevelObjectView characterView, List<LevelObjectView> deathZones, List<LevelObjectView> winZones)
        {
            _startPosition = characterView.Transform.position;
            characterView.OnLevelObjectContact += OnLevelObjectContact;

            _characterView = characterView;
            _deathZones = deathZones;
            _winZones = winZones;
        }

        public void Dispose()
        {
            _characterView.OnLevelObjectContact -= OnLevelObjectContact;
        }

        private void OnLevelObjectContact(LevelObjectView thisView, LevelObjectView otherView)
        {
            if(_deathZones.Contains(otherView))
            {
                _characterView.Transform.position = _startPosition;
            }
        }
    }
}