﻿using UnityEngine;

namespace Lessons
{
    public class AimingMuzzle
    {
        private Transform _muzzleTransform;
        private Transform _aimTransform;
        Vector3 _startMuzzleDir;

        public AimingMuzzle(Transform muzzleTransform, Transform aimTransform)
        {
            _muzzleTransform = muzzleTransform;
            _aimTransform = aimTransform;
            _startMuzzleDir = -_muzzleTransform.up;
        }

        public void Update()
        {
            var aimDir = _aimTransform.position - _muzzleTransform.position;
            var angle = Vector3.Angle(_startMuzzleDir, aimDir);
            var axis = Vector3.Cross(_startMuzzleDir, aimDir);
            _muzzleTransform.localRotation = Quaternion.AngleAxis(angle, axis);
        }
    }
}
