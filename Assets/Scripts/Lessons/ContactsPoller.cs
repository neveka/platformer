﻿using UnityEngine;

namespace Lessons
{
    public class ContactsPoller
    {
        private const float _collisionNormalThresh = 0.5f;
        private const float _velocityThresh = 0.1f;

        private ContactPoint2D[] _contacts = new ContactPoint2D[10];
        private int _contactsCount;
        private readonly Collider2D _collider2D;

        public bool IsGrounded { get; private set; }
        public bool HasLeftContacts { get; private set; }
        public bool HasRightContacts { get; private set; }
        public Vector2 GroundVelocity { get; private set; }

        public ContactsPoller(Collider2D  collider2D)
        {
            _collider2D = collider2D;
        }

        public void Update()
        {
            IsGrounded = false;
            HasLeftContacts = false;
            HasRightContacts = false;
            _contactsCount = _collider2D.GetContacts(_contacts);
            for (int i = 0; i < _contactsCount; i++)
            {
                var normal = _contacts[i].normal;
                var rigidBody = _contacts[i].rigidbody;
                var rigidBodyMoves = rigidBody != null && rigidBody.bodyType != RigidbodyType2D.Static;
                var isHorizontalStatic = !rigidBodyMoves || Mathf.Abs(rigidBody.velocity.x) < _velocityThresh;

                if (normal.y > _collisionNormalThresh)
                {
                    IsGrounded = true;
                    GroundVelocity = rigidBodyMoves? rigidBody.velocity : Vector2.zero;
                }
                if (normal.x > _collisionNormalThresh && isHorizontalStatic) HasLeftContacts = true;
                if (normal.x < -_collisionNormalThresh && isHorizontalStatic) HasRightContacts = true;
            }
        }
    }
}
