using UnityEngine;
using System.Collections;
using SpriteUtils;

public class DragonSprite : AnimatedSprite, ILevelObject
{
	public float angle = 10;
	void Update ()
	{
		transform.localRotation = Quaternion.Euler(new Vector3(0, 0, angle*Mathf.Sin(Time.time*2)));
	}

    public void SetVisible(bool visible)
    {
        gameObject.SetActive(visible);
    }

    public int GetId()
    {
        return gameObject.GetInstanceID();
    }
}

