using UnityEngine;
using System.Collections;
using SpriteUtils;

public class YoshySprite : AnimatedSprite
{
	public override void Awake()
	{
		if(spriteManager == null)
			spriteManager = GameObject.FindGameObjectWithTag("YoshySpriteManager").GetComponent<SpriteManager>();		
		base.Awake();
		_loop = true;
		_length = 8;
	}	
}
