using UnityEngine;
using System.Collections;

namespace SpriteUtils
{
    public class AnimatedSprite : MonoBehaviour
    {
        public SpriteManager spriteManager;
        public float frameWidth = 63;
        public float frameHeight = 63;
        public float yFrameOffset = 15;
        public float spriteSize = 0.03f;

        protected int _track = 0;
        protected bool _loop = false;
        protected int _length = 8;
        protected float _speed = 10;
        protected float _counter = 0;
        protected Sprite _sprite;
        protected Transform _transform;

        public virtual void Awake()
        {
            _transform = transform;
        }

        protected virtual void OnEnable()
        {
            _sprite = spriteManager.AddSprite(gameObject, // The game object to associate the sprite to
                                         spriteSize * frameWidth,       // The width of the sprite
                                         spriteSize * frameHeight,      // The height of the sprite
                                         0,             // Left pixel
                                         (int)(yFrameOffset + frameHeight),     // Bottom pixel
                                         (int)frameWidth,   // Width in pixels
                                         (int)frameHeight,  // Height in pixels
                                         false);        // Billboarded?
        }

        public virtual void Update()
        {
            if (_counter < _length)
                _counter += Time.deltaTime * _speed;

            if (_counter > _length && _loop)
                _counter %= _length;

            if (_counter < _length)
                _sprite.lowerLeftUV = spriteManager.PixelCoordToUVCoord(
                    new Vector2(frameWidth * ((int)_counter),
                                 yFrameOffset + frameHeight * (1 + _track)));
        }

        protected virtual void OnDisable()
        {
            spriteManager.RemoveSprite(_sprite);
        }

        public int animationTrack
        {
            get { return _track; }
        }

        public void Turn(bool right)
        {
            _transform.localScale = new Vector3(right ? 1 : -1, 1, 1);
        }
    }
}