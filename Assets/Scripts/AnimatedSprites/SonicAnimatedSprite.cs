using SpriteUtils;

public class SonicAnimatedSprite : AnimatedSprite 
{
    public enum Animations
    {
        walk = 0
    }
    private int[] lenthes = { 4, 1, 1, 9, 6, 1 };
	public void StartAnimation(int track, bool loop, float speed)
	{
		if(_track!=track)
		{
			_loop = loop;
			_speed = speed;
			_counter = 0;
			_track = track;
			_length = lenthes[_track];
		}
	}
}
