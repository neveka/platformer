﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Gameplay
{
    private WalkerComponent _heroWalker;
    private ShooterComponent _heroShooter;
    private ICharacterVisual _heroVisual;
    private Tetris _tetris;
    private List<IDisposable> _disposables = new List<IDisposable>();

    private ILevelEvents _levelEvents;
    private ILevelLoader _levelLoader;

    private string _levelOnComplete = "";
    private string _oldLevel = "";

    public Gameplay(ILevelEvents levelEvents, ILevelLoader levelLoader, ICharacterVisual heroVisual)
    {
        _levelEvents = levelEvents;
        _levelLoader = levelLoader;
        _heroVisual = heroVisual;

        _levelEvents.OnStartGame += OnStartGame;
        _levelEvents.OnStartLevel += OnStartLevel;
        _levelEvents.OnFinishLevel += OnFinishLevel;
    }

    public void OnStartGame()
    {
        Time.timeScale = 1.5f;
        _heroWalker = new WalkerComponent(_heroVisual, _levelEvents);
        _heroWalker.OnPortalEntered += OnPortalEntered;
        _heroShooter = new ShooterComponent(_heroVisual, _heroWalker, _levelEvents);
        _heroShooter.OnFiguresCountChanged += OnFiguresCountChanged;

        _oldLevel = "levelMario";
        _levelLoader.LoadLevel(_oldLevel);
    }

    public void OnStartLevel()
    {
        var tetrisGameObject = GameObject.FindGameObjectWithTag("TetrisZone");
        var startPosition = GameObject.FindGameObjectWithTag("StartPoint").transform.position;
        _tetris = tetrisGameObject?.GetComponent<Tetris>();
        if (_tetris != null) _tetris.OnFailed += OnLevelFailed;
        _heroWalker.SetStartPosition(startPosition, _tetris == null);
        _heroShooter.SetTetris(_tetris);
        _heroVisual.Camera.gameObject.SetActive(_tetris == null);

        var characterVisuals = GameObject.FindObjectsOfType<CharacterLevelObject>();
        foreach (var characterVisual in characterVisuals)
        {
            if (characterVisual == _heroVisual) continue;
            var enemy = new EnemyComponent(characterVisual, _levelEvents);
            _disposables.Add(enemy);
        }
    }

    public void OnFinishLevel()
    {
        if (_tetris != null) _tetris.OnFailed -= OnLevelFailed;
        for (int i = 0; i < _disposables.Count; i++)
        {
            _disposables[i].Dispose();
            _disposables[i] = null;
        }
        _disposables.Clear();
    }

    public void OnFiguresCountChanged(int count)
    {
        //gui.counter = count;
        if (count == 0 && _levelOnComplete != "")
        {
            _oldLevel = _levelOnComplete;
            _levelLoader.LoadLevel(_levelOnComplete);
        }
    }

    public void OnPortalEntered(Portal portal)
    {
        Debug.Log("levelOnComplete " + portal.level);
        _levelLoader.LoadLevel(portal.level);
        _levelOnComplete = portal.levelOnComplete;
    }

    public void OnLevelFailed()
    {
        _levelLoader.LoadLevel(_oldLevel);
    }
}