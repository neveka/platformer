using UnityEngine;
using System.Collections;
using System.Collections.Generic;
	
public class Gui : MonoBehaviour
{
	public int counter = 0;
	void Start ()
    {
		DontDestroyOnLoad (this);
	}
	
	void OnGUI()
	{
		GUI.Label(new Rect(10, 10, 100, 20), counter+" figures");
	}
}
