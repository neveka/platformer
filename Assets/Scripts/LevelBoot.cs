using System;
using UnityEngine;

public interface ILevelEvents
{
    Action OnStartGame { get; set; }
    Action OnStartLevel { get; set; }
    Action OnFinishLevel { get; set; }
    Action OnFixedUpdate { get; set; }
}

public interface ILevelLoader
{
    void LoadLevel(string levelName);
}

public class LevelBoot : MonoBehaviour, ILevelLoader, ILevelEvents
{
    public Action OnStartGame { get; set; }
    public Action OnStartLevel { get; set; }
    public Action OnFinishLevel { get; set; }
    public Action OnFixedUpdate { get; set; }

	public Gui gui;
	public CharacterLevelObject hero;

    private Gameplay _game;
    private bool _isLoading;

	private void Start() 
	{
        _game = new Gameplay(this, this, hero);
        OnStartGame?.Invoke();
    }

    private void FixedUpdate()
    {
        if (_isLoading) return;
        OnFixedUpdate?.Invoke();
    }

    public void LoadLevel(string LevelName)
    {
        OnFinishLevel?.Invoke();
        gui.gameObject.SetActive(false);
        _isLoading = true;
        Application.LoadLevel(LevelName);
    }

    private void OnLevelWasLoaded(int level)
	{
        OnStartLevel?.Invoke();
		gui.gameObject.SetActive(true);
        _isLoading = false;
    }
}
