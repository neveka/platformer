using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GeneratedLevel : MonoBehaviour 
{
    public float side = 0.3f;
    public GameObject cubePrefab;
	public GameObject figurePrefab;
	public GameObject railPrefab;
	public GameObject portalPrefab;
	private GameObject[][] _matrix = null; 

	List<Vector2> allVariants = new List<Vector2>();
	List<Vector2> variants = new List<Vector2>();

	// Use this for initialization
	void Start () 
	{
		Transform enterPoint = transform.Find("EnterPoint");
		Vector2 p = new Vector2( enterPoint.localPosition.x/side, enterPoint.localPosition.y/side);

		int w = (int)(GetComponent<BoxCollider>().size.x/side);
		int h = (int)(GetComponent<BoxCollider>().size.y/side);//?
		Debug.Log("Level dimentions "+w+" "+h);
		_matrix = new GameObject[w][];
		for (int x = 0; x < w; x++) 
		{
   			_matrix [x] = new GameObject[h];
			for(int y=0; y<h; y++)
			{
				_matrix[x][y] = null;
				//if(y==0)
				//	AddBlock(x, y);
			}
		}
		//allVariants.Add(new Vector2(1, -1));
		//allVariants.Add(new Vector2(-1, -1));
		allVariants.Add(new Vector2(1, 0));
		allVariants.Add(new Vector2(-1, 0));
		allVariants.Add(new Vector2(2, 0));
		allVariants.Add(new Vector2(-2, 0));
		allVariants.Add(new Vector2(3, 0));
		allVariants.Add(new Vector2(-3, 0));
		//allVariants.Add(new Vector2(4, 0));
		//allVariants.Add(new Vector2(-4, 0));
		allVariants.Add(new Vector2(1, 1));
		allVariants.Add(new Vector2(-1, 1));
		allVariants.Add(new Vector2(2, 1));
		allVariants.Add(new Vector2(-2, 1));
		allVariants.Add(new Vector2(1, 2));
		allVariants.Add(new Vector2(-1, 2));		
		allVariants.Add(new Vector2(3, 1));
		allVariants.Add(new Vector2(-3, 1));
		allVariants.Add(new Vector2(4, 1));
		allVariants.Add(new Vector2(-4, 1));
		Vector2 oldP = p;
		for(int i=0; i<20; i++)
		{
			//Debug.Log(p);
			AddBlock((int)p.x, (int)p.y);	
			if(oldP.x<p.x)
				for(int j=(int)oldP.x; j<=(int)p.x; j++)
				{
					AddEmptiness(j, (int)p.y, false);
					AddEmptiness(j, (int)p.y+1, true);
					if(j >= (int)p.x-1 || p.y==oldP.y)//temp
						AddEmptiness(j, (int)p.y+2, true);
				}
			if(oldP.x>p.x)
				for(int j=(int)oldP.x; j>=(int)p.x; j--)
				{
					AddEmptiness(j, (int)p.y, false);
					AddEmptiness(j, (int)p.y+1, true);
					if(j <= (int)p.x+1 || p.y==oldP.y)//temp
						AddEmptiness(j, (int)p.y+2, true);
				}
			variants.Clear();
			for(int j=0; j<allVariants.Count; j++)
			{
				//Debug.Log((p+allVariants[j]));
				if(p.x+allVariants[j].x>=0 && p.x+allVariants[j].x<w &&
				   p.y+allVariants[j].y>=0 && p.y+allVariants[j].y<h-1 &&
					_matrix[(int)(p.x+allVariants[j].x)][(int)(p.y+allVariants[j].y)]==null)
				{
					variants.Add(allVariants[j]);
					//Debug.Log(allVariants[j]);
				}
			}
			oldP = p;
			if(variants.Count == 0)
				break;
			p = p+variants[Random.Range(0, variants.Count)];
		}
		AddPrefab((int)oldP.x, (int)oldP.y+2, portalPrefab, Vector3.zero);
	}

	void AddBlock(int x, int y)
	{
		_matrix[x][y] = AddPrefab(x, y, cubePrefab, Vector3.zero);
		AddPrefab(x, y, railPrefab, new Vector3(0, 0.8f, 0.5f));
	}
	
	GameObject AddPrefab(int x, int y, GameObject prefab, Vector3 offset)
	{
		if(prefab == null)
			return null;
		GameObject go = (GameObject)Instantiate(prefab);
		go.transform.parent = transform;
		go.transform.localPosition = (new Vector3(x+0.5f, y+0.5f, 0)+offset)*side;
		return go;
	}

	void AddEmptiness(int x, int y, bool withFigure)
	{
		if(x>=0 && x<_matrix.Length && y>=0 && y<_matrix[0].Length && _matrix[x][y] == null)
		{
			GameObject blanc = new GameObject("Empty");
			blanc.transform.parent = transform;
			blanc.transform.localPosition = new Vector3((x+0.5f)*side, (y+0.5f)*side, 0);
			_matrix[x][y] = blanc;
			if(withFigure && Random.Range(0,4)==0)
			{
				GameObject figure = (GameObject)Instantiate(figurePrefab);
				figure.transform.parent = transform;
				figure.transform.localPosition = new Vector3((x+0.5f)*side, (y+0.5f)*side, 0);
			}
		}	
	}
}
