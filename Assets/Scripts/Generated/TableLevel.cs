using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class TableLevel : MonoBehaviour 
{
	public string asset;
	public float side = 0.3f;
	public string [] simbols;
	public GameObject [] prefabs;
	void Awake () 
	{
		Dictionary<string, GameObject> dictionary = new Dictionary<string, GameObject>();
		for(int i=0; i<simbols.Length; i++)
			dictionary.Add(simbols[i], prefabs[i]);

		Transform enterPoint = transform.Find("EnterPoint");
		Vector2 p = new Vector2( enterPoint.localPosition.x/side, enterPoint.localPosition.y/side);
		
		TextAsset textAsset = Resources.Load(asset, typeof(TextAsset)) as TextAsset;
		string text = textAsset.text;
		int x = 0;
		int y = 0;
		int index0 = 0;
		while (index0<text.Length && text.IndexOf('\n', index0)!=-1) 
		{
			string line = text.Substring(index0, text.IndexOf('\n', index0)-index0);
			int index = 0;
			while(index<line.Length && line.IndexOf(',', index)!=-1)
			{
				string simbol = line.Substring(index, line.IndexOf(',', index)-index);
				//Debug.Log(simbol);
				if(dictionary.ContainsKey(simbol))
					AddPrefab(-x, -y, dictionary[simbol], Vector3.zero);
				else if(dictionary.ContainsKey(simbol.ToLower()))
					AddPrefab(-x, -y, dictionary[simbol.ToLower()], Vector3.zero);
				index = line.IndexOf(',', index)+1;
				x++;
			}
			//Debug.Log(line);
			y++;
			x = 0;
			index0 = textAsset.text.IndexOf('\n', index0)+1;
		}		
	}
	
	GameObject AddPrefab(int x, int y, GameObject prefab, Vector3 offset)
	{
		if(prefab == null)
			return null;
		GameObject go = (GameObject)Instantiate(prefab);
		go.transform.parent = transform;
		go.transform.localPosition = (new Vector3(x+0.5f, y+0.5f, 0)+offset)*side;
		return go;
	}	
}
